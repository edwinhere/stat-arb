library(urca)
library(Quandl)
library(xts)
library(PerformanceAnalytics)
library(plyr)
Quandl.api_key("KRHvv5HKrrhb5_G7LLN4")
symbols <- c(
  "CHRIS/CME_ES",
  "CHRIS/CME_NK",
  "CHRIS/CME_NQ",
  "CHRIS/ICE_TF",
  "CHRIS/CME_AD",
  "CHRIS/CME_BP",
  "CHRIS/CME_CD",
  "CHRIS/CME_EC",
  "CHRIS/CME_JY",
  "CHRIS/CME_SF",
  "CHRIS/CME_E7",
  "CHRIS/CME_LN",
  "CHRIS/CME_LC",
  "CHRIS/CME_CL",
  "CHRIS/CME_QM",
  "CHRIS/CME_NG",
  "CHRIS/CME_QG",
  "CHRIS/CME_C",
  "CHRIS/CME_W",
  "CHRIS/CME_S",
  "CHRIS/CME_SM",
  "CHRIS/CME_BO",
  "CHRIS/CME_YM",
  "CHRIS/CME_TU",
  "CHRIS/CME_FV",
  "CHRIS/CME_TY",
  "CHRIS/CME_US",
  "CHRIS/CME_UL",
  "CHRIS/CME_ED",
  "CHRIS/CME_GC"
)
symbols.1.4 <- sapply(
  symbols,
  function(x) {
    paste(x, "1.4", sep = "")
  }
)
all.data <- Quandl(
  symbols.1.4, 
  start_date=Sys.Date() - 183,
  type = "xts"
)
names(all.data) <- symbols
without.na <- all.data[complete.cases(all.data),]
separated <- lapply(
  names(without.na),
  function(x) {
    without.na[, x]
  }
)
names(separated) <- names(without.na)
standard.length <- nrow(separated$`CHRIS/CME_ES`)
separated <- compact(lapply(
  separated,
  function(x) {
    if(nrow(x) == standard.length) {
      x
    } else {
      NULL
    }
  }
))
training.xts <- lapply(
  separated, 
  function(x) {
    x[1:(standard.length/2),]
  }
)
testing.xts <- lapply(
  separated, 
  function(x) {
    x[((standard.length/2)+1):standard.length,]
  }
)
baskets <- t(combn(names(training.xts), m = 2))
test.results <- do.call("rbind", apply(
  baskets,
  1,
  function(x) {
    basket.list <- training.xts[x]
    basket <- Reduce(function(a, b) merge.xts(a, b), basket.list)
    jotest <- ca.jo(basket, type="trace", K=3, ecdet="none", spec="longrun")
    half.life <- NA
    prices <- basket[,1]*jotest@V[1,1] + basket[,2]*jotest@V[2,1] # To be changed for more legs
    prices.lag <- lag.xts(prices, na.pad = TRUE)
    prices.diff <- diff.xts(prices, na.pad = TRUE)
    fit <- lm(prices.diff ~ prices.lag, data = prices)
    half.life <- abs(log(2) / coefficients(fit)[2])
    result <- data.frame (
      weight.1 = jotest@V[1,1],
      leg.1 = x[1],
      weight.2 = jotest@V[2,1],
      leg.2 = x[2],
      half.life = round(half.life),
      is.cointegrated.99 = all(abs(jotest@teststat) > jotest@cval[,3]),
      is.cointegrated.95 = all(abs(jotest@teststat) > jotest@cval[,2]),
      is.cointegrated.90 = all(abs(jotest@teststat) > jotest@cval[,1])
    )
    row.names(result) <- 1:nrow(result)
    result
  }
))

blank <- function() {
  output <- data.frame (
    "basket" = NA,
    "weight.1" = NA,
    "leg.1" = NA,
    "weight.2" = NA,
    "leg.2" = NA,
    "is.cointegrated.99" = NA,
    "is.cointegrated.95" = NA,
    "is.cointegrated.90" = NA,
    "half.life" = NA,
    "cumulative.returns" = NA,
    "max.drawdown" = NA,
    "kelly.ratio" = NA,
    "is.positive.kelly" = NA,
    "lot.size" = NA,
    "last.signal" = NA,
    "last.true.signal" = NA,
    "last.signal.date" = NA,
    "adjusted.weight.1" = NA,
    "adjusted.weight.2" = NA,
    "mean" = NA,
    "sd" = NA,
    "fees" = NA
  )
  names(output) <- c(
    "basket",
    "true.weight.1",
    "leg.1",
    "true.weight.2",
    "leg.2",
    "is.cointegrated.99",
    "is.cointegrated.95",
    "is.cointegrated.90",
    "half.life",
    "cumulative.returns",
    "max.drawdown",
    "kelly.ratio",
    "is.positive.kelly",
    "lot.size",
    "last.signal",
    "last.true.signal",
    "last.signal.date",
    "adjusted.weight.1",
    "adjusted.weight.2",
    "mean",
    "sd",
    "fees"
  )
  row.names(output) <- 1:nrow(output)
  output
}

backtest.results <- do.call("rbind", apply(
  test.results,
  1,
  function(basket) {
    true.weight.1 <- as.double(basket["weight.1"])
    true.weight.2 <- as.double(basket["weight.2"])
    weight.1 <- signif(as.double(basket["weight.1"]), 1)
    leg.1 <- basket["leg.1"]
    weight.2 <- signif(as.double(basket["weight.2"]), 1)
    leg.2 <- basket["leg.2"]
    half.life <- as.numeric(basket["half.life"])
    multiplier <- 10^abs(floor(log10(abs(weight.2))))
    weight.1 <- multiplier * weight.1
    weight.2 <- multiplier * weight.2
    
    adj.component.prices <- merge.xts((testing.xts[leg.1][[1]] * weight.1),
                                      (testing.xts[leg.2][[1]] * weight.2))
    adj.prices <- adj.component.prices[,1] + adj.component.prices[,2]
    actual.component.prices <- merge.xts((testing.xts[leg.1][[1]] * weight.1),
                                         (testing.xts[leg.2][[1]] * weight.2))
    actual.prices <- actual.component.prices[,1] + actual.component.prices[,2]
    if(half.life > 1 && half.life < nrow(adj.prices)) {
      average <- rollmean(adj.prices, half.life, fill = NA, align = "right")
      stddev <- rollapplyr(adj.prices, half.life, sd, fill = NA, align = "right")
      merged.with.na <- merge.xts(adj.prices, average, stddev)
      names(merged.with.na) <- c("adj.prices", "average", "stddev")
      merged <- merged.with.na[complete.cases(merged.with.na),]
      zscores <- (merged[,"adj.prices"] - merged[,"average"]) / merged[,"stddev"]
      predictions <- lag.xts(trunc(-zscores))
      inter.day.change <- diff.xts(actual.prices)
      merged.returns <- merge.xts(predictions, inter.day.change)
      returns <- merged.returns[,1] * merged.returns[,2]
      cumulative.returns <- Return.cumulative(returns, geometric = FALSE)
      returns <- na.omit(returns)
      max.drawdown <- maxDrawdown(returns)
      kelly.ratio <- KellyRatio(returns)
      last.signal <- -round(tail(zscores, n = 1))
      last.true.signal <- -tail(zscores, n = 1)
      last.signal.date <- index(last.true.signal)
      last.mean <- tail(merged[,"average"], n = 1)
      last.sd <- tail(merged[,"stddev"], n = 1)
      
      output <- data.frame (
        "basket" = sprintf(
          "%s * %s %s %s * %s", 
          weight.1, 
          leg.1, 
          ifelse(weight.2 < 0, '', '+'), 
          weight.2, 
          leg.2),
        "true.weight.1" = true.weight.1,
        "leg.1" = leg.1,
        "true.weight.2" = true.weight.2,
        "leg.2" = leg.2,
        "is.cointegrated.99" = basket["is.cointegrated.99"],
        "is.cointegrated.95" = basket["is.cointegrated.95"],
        "is.cointegrated.90" = basket["is.cointegrated.90"],
        "half.life" = half.life,
        "cumulative.returns" = cumulative.returns,
        "max.drawdown" = max.drawdown,
        "kelly.ratio" = kelly.ratio,
        "is.positive.kelly" = kelly.ratio > 0,
        "lot.size" = abs(weight.1) + abs(weight.2),
        "last.signal" = last.signal,
        "last.true.signal" = last.true.signal,
        "last.signal.date" = last.signal.date,
        "adjusted.weight.1" = weight.1,
        "adjusted.weight.2" = weight.2,
        "mean" = last.mean,
        "sd" = last.sd,
        "fees" = (((abs(weight.1) + abs(weight.2)) * 3 * 0.81) + 10.65) * 1.07 * 2.0
      )
      names(output) <- c(
        "basket",
        "true.weight.1",
        "leg.1",
        "true.weight.2",
        "leg.2",
        "is.cointegrated.99",
        "is.cointegrated.95",
        "is.cointegrated.90",
        "half.life",
        "cumulative.returns",
        "max.drawdown",
        "kelly.ratio",
        "is.positive.kelly",
        "lot.size",
        "last.signal",
        "last.true.signal",
        "last.signal.date",
        "adjusted.weight.1",
        "adjusted.weight.2",
        "mean",
        "sd",
        "fees"
      )
      row.names(output) <- 1:nrow(output)
      output
    } else {
      blank()
    }
  }
))
backtest.results <- backtest.results[complete.cases(backtest.results),]
backtest.results <- backtest.results[backtest.results["is.positive.kelly"] == TRUE,]
backtest.results <- backtest.results[order(-backtest.results["kelly.ratio"]),]
backtest.results <- backtest.results[backtest.results$is.cointegrated.90 != FALSE,]
#backtest.results <- backtest.results[backtest.results$sd * 25 > backtest.results$fees,] # The 25 is because we assume the options are all 25 delta.
write.csv(backtest.results, file = paste("/tmp/futures-", Sys.Date(),".csv", sep = ""))